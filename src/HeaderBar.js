import React from 'react';
import {View,Text,StatusBar} from 'react-native';


const HeaderBar = (props) =>{
  return(
    <View>
        <View style = {styles.headerContainer}>
            <Text style={styles.textStyle}>{props.name}</Text>
        </View>
    </View>    
  )
}

const styles={
    headerContainer:{
        backgroundColor:'#CD5C5C', 
        height:60,
        justifyContent:"center",
        alignItems:"center",
        shadowColor: 'black',
        shadowOffset: {width:0,height:2},
        shadowOpacity: 0.2,    
        elevation:2,
        position:'relative'
     },
    textStyle:{
        color:'white',
        fontSize: 20,   
        fontWeight: 'bold',
       
    }
}

export default HeaderBar ;