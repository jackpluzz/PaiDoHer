import React, { Component } from 'react';
import { View, Text,TouchableOpacity,ScrollView,Image,ImageBackground} from 'react-native';


export default class CardMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      
      <ScrollView style = {styles.container}>


        
       <View style = {styles.cardStyle}>
       <TouchableOpacity onPress={() => this.props.navigation.navigate('PlaceDetailScreen')} >
            <ImageBackground style={styles.ImageStyle} source={{uri: 'https://blog.traveloka.com/source/uploads/sites/5/2018/01/%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%95%E0%B8%81%E0%B8%82%E0%B8%B8%E0%B8%99%E0%B8%81%E0%B8%A3%E0%B8%93%E0%B9%8C_%E0%B9%80%E0%B8%8A%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%A3%E0%B8%B2%E0%B8%A201.jpg'}}>      
              
              <View style = {styles.textBanner}>
                <Text style={styles.fontstyle}>น้ำตกขุนกรณ์</Text>
              </View>
              
          
            </ImageBackground>
          </TouchableOpacity>

           <TouchableOpacity >
            <ImageBackground style={styles.ImageStyle} source={{uri: 'http://www.nanglae.go.th/images/information/10849728_577676148998553_460543740457529264_n.jpg'}}>      
              
              <View style = {styles.textBanner}>
                <Text style={styles.fontstyle}>น้ำตกนางแลใน</Text>
              </View>
              
          
            </ImageBackground>
          </TouchableOpacity>

          <TouchableOpacity >
            <ImageBackground style={styles.ImageStyle} source={{uri: 'http://travel.mthai.com/app/uploads/2014/07/%E0%B9%82%E0%B8%9B%E0%B9%88%E0%B8%87%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%9A%E0%B8%B2%E0%B8%97.jpg'}}>      
              
              <View style = {styles.textBanner}>
                <Text style={styles.fontstyle}>น้ำตกโป่งพระบาท</Text>
              </View>
              
          
            </ImageBackground>
          </TouchableOpacity>

          <TouchableOpacity >
            <ImageBackground style={styles.ImageStyle} source={{uri: 'http://1.bp.blogspot.com/-S4OnHUtNxWQ/U_B_17YwvSI/AAAAAAAAK2w/wH49rKZe4L8/s1600/DSC_1915.JPG'}}>      
              
              <View style = {styles.textBanner}>
                <Text style={styles.fontstyle}>น้ำตกห้วยแก้ว</Text>
              </View>
              
          
            </ImageBackground>
          </TouchableOpacity>
          
       </View>

    

      </ScrollView>
    );
  }
}


const styles ={
    container:{
        flex: 1,
        // flexDirection: 'column',
        // justifyContent:'space-between'
    },
    cardStyle:{
        // backgroundColor:'red',
        marginBottom: 5,
    },
    ImageStyle:{
      height:200,
      marginBottom: 5,
      justifyContent:'flex-end'
      
    },
    textBanner:{
      // backgroundColor:'rgba(0, 0, 0, .5)',
     alignItems: 'flex-end',
     
    },
    fontstyle:{
      fontSize:20,
      fontWeight: 'bold',
      color:'white'
    }
    
}
