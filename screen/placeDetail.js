import React, { Component } from 'react';
import { ScrollView, View, Text, ImageBackground, TouchableOpacity,Button, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MTIcon from 'react-native-vector-icons/MaterialIcons';
import MAIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import EIcon from 'react-native-vector-icons/EvilIcons'

export default class PlaceDetail extends Component {
  static navigationOptions = {
    header: null,
    tabBarVisible: false,
  };

  render() {
    return (
      <ScrollView>
        <View style={{ flex: 1 }}>
          <View style={{ backgroundColor: 'white' }}>
            <ImageBackground style={{ backgroundColor: 'white', height: 200, width: 415, marginBottom: 5 }}
              source={{uri: 'https://blog.traveloka.com/source/uploads/sites/5/2018/01/%E0%B8%99%E0%B9%89%E0%B8%B3%E0%B8%95%E0%B8%81%E0%B8%82%E0%B8%B8%E0%B8%99%E0%B8%81%E0%B8%A3%E0%B8%93%E0%B9%8C_%E0%B9%80%E0%B8%8A%E0%B8%B5%E0%B8%A2%E0%B8%87%E0%B8%A3%E0%B8%B2%E0%B8%A201.jpg'}}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('MenulistScreen')}>
                <EIcon style={{ color: 'white' }} name="chevron-left" size={50} />
              </TouchableOpacity>
            </ImageBackground></View>
          <View style={{ backgroundColor: 'white', flexDirection: 'row' }}>
            <Text style={{ fontSize: 20, marginLeft: 28, marginBottom: 5, color: '#555555' }}>น้ำตกขุนกรณ์</Text>
            <EIcon style={{ color: "hsl(204, 86%, 53%)", marginTop: 7 }} name="location" size={20} />
            <Text style={{ fontSize: 18, marginLeft: 5, marginBottom: 5, color: '#555555' }}>ห้วยชมภู, เมืองเชียงราย</Text>
          </View>

          <View>
            <View style={{ flexDirection: 'row', marginLeft: 5, marginTop: 5 }}>
              <Image style={{ height: 110, width: 130, marginBottom: 5, marginLeft: 2 }}
                source={require('../photo/banner-doimaesalong.jpg')}></Image>

              <Image style={{ height: 110, width: 130, marginBottom: 5, marginLeft: 2 }}
                source={require('../photo/banner-doimaesalong.jpg')}></Image>

              <Image style={{ height: 110, width: 130, marginBottom: 5, marginLeft: 2 }}
                source={require('../photo/banner-doimaesalong.jpg')}></Image>

            </View>
          </View>
          <View style={{ backgroundColor: 'white' }}>
            <View style={{ flexDirection: 'column', marginLeft: 20, marginBottom: 20, marginTop: 5, justifyContent: 'space-around' }}>
              <View style={{ flexDirection: 'row' }}>

              </View>


              <Text style={{ fontSize: 15 }}>Overview :ลำน้ำแม่กรณ์นี้เองที่เป็นต้นธารของน้ำตกขุนกรณ์โดยน้ำตกสายนี้สูง 70 เมตร มีน้ำไหลตลอดทั้งปี </Text>
              <Text style={{ fontSize: 15 }}>เบอร์โทรติดต่อ :053-726-368</Text>
              <Text style={{ fontSize: 15 }}>การเดินทาง :รถประจำทางสองแถวเชียงราย-บ้านปางริมกรณ์</Text>
              <Text style={{ fontSize: 15 }}>unseen :จุดเด่นคือความใสของน้ำที่สามารถมองทะลุถึงเบื้องล่างเย้ายวนให้ใครก็ตามที่ได้มาเยือนแล้ว ต้องอดใจไม่ไหวลงเล่นน้ำทุกครั้งไป</Text>
              <Text style={{ fontSize: 15 }}>เวลา เปิด-ปิด :Sunday-Saturday 08.00 - 17.00</Text>
              <Text style={{ fontSize: 15 }}>ค่าเข้าชม :50 บาท </Text>
            </View>
          </View>

          <View>
            <ImageBackground style={{ marginTop: 5, marginLeft: 5, marginBottom: 10, height: 350, width: null }}
              source={{uri:'https://goo.gl/LHjUSJ'}}></ImageBackground>

          </View>

          {/* <Button title="Goto Category" onPress={()=> this.props.navigation.navigate('CategoryScreen')}/> */}
        </View>
      </ScrollView>
    );
  }
}
