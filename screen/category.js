import React, { Component } from 'react';
import { View, ScrollView, Text, Image, ImageBackground, TouchableOpacity } from 'react-native';
import HeaderBar from "../src/HeaderBar";
import firebase from 'firebase';


export default class Category extends Component {
  static navigationOptions = {
    header: null,
    tabBarVisible: false,
  };

  componentDidMount(){
    this.fetchBlog();
  }

  constructor(props) {
    super(props);
    this.state = {
      location:[] ,
      modalVisible: false,
    }
  }
  async fetchBlog(){
    let data = [];
    let datt = []
    let count ;
    for(count =0 ; count<=15;count ++){
      const resp = await fetch('https://paidoher.firebaseio.com/location.json');
    data = firebase.database().ref('/location/-LKDHODapnM6N_YinMpD/'+count);
   // console.warn("TEst data", data);
    datt = await resp.json();
    console.warn("TEst data", data.val());
    }
    console.warn("test==>",datt);

  // await firebase.database().ref('/location/-LKDHODapnM6N_YinMpD').on('value',snapshot => {
    //   console.log(snapshot);
    //   snapshot.forEach(
    //     snap=>{
    //       // let value = snap.val();
    //       // value.id = snap.key;
    //       // data.push(value); 
    //       console.log("Test",snap.val());
    //     });
          // data.reverse();
          // this.setState({location:data})
      //    console.warn("TEst data", this.state.location)
    //});

  }

       




  render() {
    
    return (
      <ScrollView>
        <View style={{ flex: 1, }} >
          <HeaderBar name={"CATEGORY"} />
          {/* <Button title="Goto Category" onPress={()=> this.props.navigation.navigate('CategoryScreen')}/> */}
          <View style={{ flexDirection: 'row', marginLeft: 10 }}>
            
            <View style={{ flexDirection: 'column'}}>

              <TouchableOpacity onPress={() => this.props.navigation.navigate('MenulistScreen')} >
                <ImageBackground style={{ height: 200, width: 190, marginBottom: 15, marginTop: 15, borderRadius: 5 }}
                  source={{uri:'http://www.nanglae.go.th/images/information/64277_577676125665222_5698381021448193261_n.jpg'}}>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>WATERFALL</Text>
                </ImageBackground>

              </TouchableOpacity>

              <TouchableOpacity style={{borderRadius:20 }} onPress={() => this.props.navigation.navigate('MenulistScreen')} >
                <ImageBackground style={{ height: 250, width: 190}}
                  source={{uri:'http://www.govivigo.com/content/ideas/5633/56339a3dec60adb92a8b462c-2-full-ideas.jpg'}}>
                  <View>
                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>TEMPLE</Text>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            </View>

            <View style={{ flexDirection: 'column', marginRight: 15 }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('MenulistScreen')} >
                <ImageBackground style={{ height: 250, width: 190, marginLeft: 10, marginRight: 10, marginBottom: 15, marginTop: 15, borderRadius: 5 }}
                  source={require('../photo/banner-doimaesalong.jpg')}>
                                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>MOUNTAIN</Text>
                </ImageBackground>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('MenulistScreen')} >
                <ImageBackground style={{ height: 200, width: 190, marginLeft: 10, marginRight: 10, marginTop: 1, borderRadius: 5 }}
                 source={{uri:'https://goo.gl/hEQo5G'}}>
                                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>CAFE</Text>

                </ImageBackground>
              </TouchableOpacity>

               <TouchableOpacity>
                <ImageBackground style={{ height: 100, width: 200, marginLeft: 10, marginRight: 10, marginTop: 5, borderRadius: 5 }}
                 source={{uri:'https://goo.gl/V3pf7u'}}>
                                    <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'white' }}>Insurance</Text>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white' }}>Comming Soon!</Text>

                </ImageBackground>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>

    );
    const style = {

    }
  }
}
